
const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers")
const auth = require("./../auth");
//check if email exists - duplicate values
/*router.get("/email-exists", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result))
})
*/


//check email
router.get("/email-exists", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result))
})

//register a user
//http://localhost:4000/api/users
router.post("/register", (req, res) => {
	userController.register(req.body).then(result => res.send(result))
})

//get all users
router.get("/", (req, res) => {
	userController.getAllUsers().then(result => res.send(result))
})


router.post("/login", (req, res) => {
	userController.login(req.body).then(result => res.send(result))
})

//retrieve user information
router.get("/details", auth.verify, (req, res) => {
	
		let userData = auth.decode(req.headers.authorization)
		//Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxOWNiOTE4MzZjZThjYzM1MTZkM2FhZCIsImVtYWlsIjoidGVzdEBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjM3NjY0MjA3fQ.D6V9vNanIDfvIi51UQ2Knt1uGDhrae2hHOBmuWFVx14

		userController.getProfile(userData).then(result => res.send(result))
})


module.exports = router;