
const express = require("express");
const router = express.Router();

const courseController = require("./../controllers/courseControllers")

//create a course
router.post("/create-course", autj.verify, (req, res) => {
	courseController.createCourse(req.body).then(result => res.send(result))
})


//retrieving all courses
router.get("/", (req, res) => {
	courseController.getAllCourses().then(result => res.send(result))
})


//retrieving only active courses
router.get("/active-courses", (req, res) => {
	courseController.getActiveCourses().then(result => res.send(result))
})

//get a specific course using findOne()
router.get("/specific-course", (req, res) => {
	courseController.getSpecificCourse(req.body).then(result => res.send(result))
})


//get specific course using findById()
//619a4bfab2670a15a51b60da
router.get("/:courseId", (req, res) => {
	courseController.getSpecificId(req.params.courseId).then(result => res.send(result))
})

//update isActive status of the course using findOneAndUpdate()
//isActive to false
router.put("/archiveCourse", (req, res) => {
	courseController.archiveCourse(req.body).then(result => res.send(result))
})
	//isActive to true
router.put("/unarchiveCourse", (req, res) => {
	courseController.unarchiveCourse(req.body).then(result => res.send(result))
})

//update isActive status of the course using findByIdAndUpdate()
	//isActive to false
router.put("/:courseId/archiveCourse", (req, res) => {
	courseController.archiveCourseById(req.params.courseId).then(result => res.send(result))
})
	//isActive to true
router.put("/:courseId/unarchiveCourse", (req, res) => {
	courseController.unarchiveCourseById(req.params.courseId).then(result => res.send(result))
})

//delete course using findOneAndDelete()
router.delete("/delete-courses", (req, res) => {

	courseController.deleteCourse(req.body).then( result => res.send(result))
})

//delete course using findByIdAndDelete()
router.delete("/:courseId/delete-coursesId", (req, res) => {

	courseController.deleteCourseById(req.params.courseId).then( result => res.send(result))
})


module.exports = router;