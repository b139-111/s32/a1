
const User = require("./../models/User");
const bcrypt = require("bcrypt");
const auth = require("./../auth");

//check if email exists - duplicate values
module.exports.checkEmail = (reqBody) => {
	const {email} = reqBody
	return User.findOne({email: email}).then( (result, error) => {
		if(result != null) {
			return 'Email already exists'
		} else {
			if(result == null){
				return true
			} else{
				return error
			}
		}
	})
}


//register a user
module.exports.register = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then( (result, error) => {
		if(result){
			return true
		} else {
			return false
		}
	})
}

//get all users
module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}



module.exports.login = (reqBody) => {
	const {email, password} = reqBody;

	return User.findOne({email: email}).then( (result, error) => {
		if(result == null) {
			return false
		} else {
			let isPasswordCorrect = bcrypt.compareSync(password, result.password) //user, server
			if(result){
				return {access: auth.createAccessToken(result)}
			} else{
				return error
			}
		}
	})
}


//Activity
//Retrieve details of the user
module.exports.getProfile = (data) => {
	console.log(data)
	const {id} = data

	return User.findById(id).then((result, error) => {
		console.log(result)
		if(result != null) {
			result.password = ""
			return result
			} else {
				return false
			}
	})
}

