
const Course = require("./../models/Course");

module.exports.createCourse = (reqBody) => {
	let newCourse = new Course({
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then( (result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.getAllCourses = () => {

	return Course.find().then( (result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}


module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then((result, err) => {
		if(err) {
			return false
		} else {
			return result
		}
	})
}


module.exports.getSpecificCourse = (reqBody) => {
	return Course.findOne({courseName: reqBody.courseName}).then((result, err) => {
		if(result == null) {
			return 'Course not existing'
		} else {

			if(result){
				return result
			} else{
				return error
			}
		}
	})
}


module.exports.getSpecificId = (params) => {
	return Course.findById(params).then((result, err) => {
		if(result == null) {
			return 'Course not existing'
		} else {

			if(result){
				return result
			} else{
				return error
			}
		}
	})
}


module.exports.archiveCourse = (reqBody) => {
		let updatedIsActive = {
		isActive: false
	}

	return Course.findOneAndUpdate({courseName: reqBody.courseName}, updatedIsActive).then(
		result => {
		if(result == null) {
			return 'Course not existing'
		} else {

			if(result){
				return result
			} else{
				return error
			}
		}
	})
}

module.exports.unarchiveCourse = (reqBody) => {
		let updatedIsActive = {
		isActive: true
	}

	return Course.findOneAndUpdate({courseName: reqBody.courseName}, updatedIsActive).then(
		result => {
		if(result == null) {
			return 'Course not existing'
		} else {

			if(result){
				return result
			} else{
				return error
			}
		}
	})
}


module.exports.archiveCourseById = (params) => {
		let updatedIsActive = {
		isActive: false
	}

	return Course.findByIdAndUpdate({_id: params}, updatedIsActive, {new: true}).then(
		result => {
		if(result == null) {
			return 'Course not existing'
		} else {

			if(result){
				return result
			} else{
				return error
			}
		}
	})
}
module.exports.unarchiveCourseById = (params) => {
		let updatedIsActive = {
		isActive: true
	}

	return Course.findByIdAndUpdate({_id: params}, updatedIsActive, {new: true}).then(
		result => {
		if(result == null) {
			return 'Course not existing'
		} else {

			if(result){
				return result
			} else{
				return error
			}
		}
	})
}


module.exports.deleteCourse = (reqBody) => {

	return Course.findOneAndDelete({courseName: reqBody.courseName}).then( result => {
		if(result == null) {
			return 'Course not existing'
		} else {

			if(result){
				return true
			} else{
				return error
			}
		}
	})
}

module.exports.deleteCourseById = (params) => {

	return Course.findOneAndDelete({_id: params}).then( result => {
		if(result == null) {
			return 'Course not existing'
		} else {

			if(result){
				return true
			} else{
				return error
			}
		}
	})
}